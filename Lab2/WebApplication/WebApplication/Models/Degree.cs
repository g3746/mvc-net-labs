﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace WebApplication.Models
{
	public class Degree
	{
		public int Id1 { get; set; }
		public int Id2 { get; set; }
		public int Id3 { get; set; }
		public int Id4 { get; set; }

		public int Deg1 { get; set; }
		public int Deg2 { get; set; }
		public int Deg3 { get; set; }
		public int Deg4 { get; set; }
	}
}
