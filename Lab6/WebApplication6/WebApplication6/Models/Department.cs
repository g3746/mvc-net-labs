﻿namespace WebApplication6.Models
{
	public class Department
	{
		public int Id { get; set; }
		public string Name { get; set; }

		public ICollection<Student> Students { get; set; }
		public ICollection<Course> Courses { get; set; }


		public Department()
		{
			Students = new List<Student>();
			Courses = new List<Course>();
		}
	}
}
