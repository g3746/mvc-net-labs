﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using WebApplication.ClassLib;
using WebApplication.Models;

namespace WebApplication.Controllers
{
	public class DeptController : Controller
	{
		public IDept db { get; set; }

		public DeptController (IDept _db)
		{
			this.db = _db;
		}

		//public IActionResult Index(int? page)
		//{
		//	DeptSearchViewModel model = new DeptSearchViewModel();
		//	int pageSize = 3;
		//	int pageIndex = 1;
		//	pageIndex = page.HasValue ? Convert.ToInt32(page) : 1;
		//	IPagedList<Dept> depts = null;
		//	depts = db.GetAllDepts().ToPagedList(pageIndex, pageSize);
		//	return this.View("Pagination", depts);
		//}

		public IActionResult Index(string searchtext, string selectedDept)
		{
			DeptSearchViewModel model = new DeptSearchViewModel();
			if (String.IsNullOrEmpty(searchtext))
			{
				model.Depts = db.GetAllDepts();
			}
			else
			{
				ViewBag.searchTxt = searchtext;
				model.SearchText = searchtext;
				model.Depts = db.GetAllDepts().FindAll(d => d.Name.Contains(searchtext));
			}
			if (selectedDept != null)
			{
				model.Depts = model.Depts.Where(a => a.Name == selectedDept).ToList();
			}
			var deptsNames = db.GetAllDepts().Select(d => d.Name).Distinct().ToList();
			model.DepartmentSelectList = new Microsoft.AspNetCore.Mvc.Rendering.SelectList(deptsNames);
			return View(model);
		}

		public IActionResult CreateDept()
		{
			return View();
		}

		public IActionResult CheckName(string name)
		{
			var res = db.GetAllDepts().Where(d => d.Name == name).FirstOrDefault();
			if (res == null)
			{
				return Json(true);
			}
			else
			{
				return Json(false);
			}
		}
		public IActionResult Add(Dept dept, IFormFile deptImg)
		{
			if (deptImg == null)
			{
				ModelState.AddModelError("ImgName", "profile image is requrird");
			}
			else
			{
				using (var s = new FileStream(@".\wwwroot\images\" + deptImg.FileName, FileMode.Create))
				{
					deptImg.CopyTo(s);
				}
				dept.ImgName = deptImg.FileName;
				db.AddDept(dept);
			}

			if (ModelState.IsValid)
			{
				return Json(dept);
			}
			else
			{
				return View("CreateDept", dept);
			}
		}

		public IActionResult Delete(int id)
		{
			db.DeleteDept(db.GetDeptById(id));
			return RedirectToAction(nameof(Index));
		}

		public IActionResult Details(int id)
		{
			var dept = db.GetDeptById(id);
			return View(db.GetDeptById(id));
		}

		public IActionResult Edit(int id)
		{
			return View(db.GetDeptById(id));
		}

		public IActionResult Update(Dept dept, IFormFile img)
		{
			int id = dept.Id;
			db.DeleteDept(db.GetDeptById(dept.Id));
			db.AddDept(dept);
			return RedirectToAction(nameof(Index));
		}
	}
}