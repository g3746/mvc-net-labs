﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using WebApplication.Models;

namespace WebApplication.ClassLib
{
	public interface IDept
	{
		void AddDept(Dept dept);
		List<Dept> GetAllDepts();
		void DeleteDept(Dept dept);
		Dept GetDeptById(int id);
	}
}
