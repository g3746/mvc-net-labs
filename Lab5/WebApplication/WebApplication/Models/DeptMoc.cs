﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using WebApplication.ClassLib;

namespace WebApplication.Models
{
	public class DeptMoc : IDept
	{
		private List<Dept> _depts = new List<Dept>
		{
			new Dept { Id = 1, Name = "One", Location = "Alex" },
			new Dept { Id = 2, Name = "Two", Location = "Alex" },
			new Dept { Id = 3, Name = "Three", Location = "Alex" }
		};

		public void AddDept(Dept dept)
		{
			_depts.Add(dept);
		}

		public List<Dept> GetAllDepts()
		{
			return _depts;
		}

		public void DeleteDept(Dept dept)
		{
			_depts.Remove(dept);
		}

		public Dept GetDeptById(int id)
		{
			var dept = _depts.Where(d => d.Id == id).FirstOrDefault();
			return dept;
		}
	}
}
