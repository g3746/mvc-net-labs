﻿using Microsoft.AspNetCore.Mvc;
using System.ComponentModel.DataAnnotations;

namespace WebApplication.Models
{
	public class Dept
	{
		[Required(ErrorMessage = "*")]
		public int Id { get; set; }

		[Remote("CheckName", "Dept", ErrorMessage = "Dept Already Exist"),
			StringLength(20, MinimumLength = 5)]
		[Required(ErrorMessage = "*")]
		public string Name { get; set; }

		[DataType(DataType.EmailAddress), RegularExpression(@"^[\w-\.]+@([\w-]+\.)+[\w-]{2,4}$",
														ErrorMessage = "Please enter a valid email address")]
		[Required(ErrorMessage = "*")]
		public string Email { get; set; }

		[DataType(DataType.Password), Required]
		public string Password { get; set; }

		[Compare("Password", ErrorMessage = "Password Dosen't Match"), Required]
		public string ComPassword { get; set; }

		public string Location { get; set; }
		
		public string ImgName { get; set; }
	}
}
