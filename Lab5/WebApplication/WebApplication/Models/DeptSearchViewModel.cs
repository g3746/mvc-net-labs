﻿using Microsoft.AspNetCore.Mvc.Rendering;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace WebApplication.Models
{
	public class DeptSearchViewModel
	{
		public List<Dept> Depts { get; set; }
		public string SearchText { get; set; }
		public string SelectedDept { get; set; }
		public SelectList DepartmentSelectList { get; set; }
	}
}
