﻿// <auto-generated> This file has been auto generated by EF Core Power Tools. </auto-generated>
#nullable disable
using System;
using System.Collections.Generic;

namespace MyApp
{
    public partial class Department
    {
        public Department()
        {
            Students = new HashSet<Student>();
        }

        public int Dept_Id { get; set; }
        public string Dept_Name { get; set; }

        public virtual ICollection<Student> Students { get; set; }
    }
}