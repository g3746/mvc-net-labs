using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.RazorPages;

namespace MyApp.Pages
{
  public class Dept_CreateModel : PageModel
  {
    private readonly ExamdbContext _context;
    [BindProperty]
    public Department Dept { get; set; }

    public Dept_CreateModel(ExamdbContext context)
    {
      _context = context;
    }

    public IActionResult OnPost()
		{
      _context.Departments.Add(Dept);
      _context.SaveChanges();
      return RedirectToPage("Dept");
		}
  }
}
