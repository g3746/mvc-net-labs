using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.RazorPages;
using Microsoft.EntityFrameworkCore;

namespace MyApp.Pages
{
  public class DetailsModel : PageModel
  {
    private readonly ExamdbContext _context;
    public Student Student { get; set; }

    public DetailsModel(ExamdbContext context)
		{
      _context = context;
		}

    public void OnGet(int id)
    {
      Student = _context.Students
        .Include(s => s.Dept)
        .Where(s => s.Std_Id == id)
        .FirstOrDefault();
    }
  }
}
