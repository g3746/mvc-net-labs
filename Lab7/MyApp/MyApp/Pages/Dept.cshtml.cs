using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.RazorPages;
using Microsoft.EntityFrameworkCore;

namespace MyApp.Pages.Dept
{
  public class DeptModel : PageModel
  {
    private readonly ExamdbContext _context;
		public List<Department> Depts { get; set; }
    public DeptModel(ExamdbContext context)
		{
      _context = context;
		}

		public void OnGet()
    {
      Depts = _context.Departments.ToList();
    }
  }
}
