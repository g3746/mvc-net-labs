using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.RazorPages;
using Microsoft.EntityFrameworkCore;

namespace MyApp.Pages
{
  public class Dept_detailsModel : PageModel
  {
    private readonly ExamdbContext _context;
		public Department Dept { get; set; }

		public Dept_detailsModel(ExamdbContext context)
		{
      _context = context;
		}

    public void OnGet(int id)
    {
      Dept = _context.Departments
        .Where(d => d.Dept_Id == id)
        .Include(d => d.Students)
        .FirstOrDefault();
    }
  }
}
