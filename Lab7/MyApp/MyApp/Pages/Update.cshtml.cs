using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.RazorPages;
using Microsoft.EntityFrameworkCore;

namespace MyApp.Pages
{
  public class UpdateModel : PageModel
  {
    [BindProperty]
    public Student Student { get; set; }
    public List<string> Depts { get; set; }
    private readonly ExamdbContext _context;

    public UpdateModel(ExamdbContext context)
		{
      _context = context;
		}

    public void OnGet(int id)
    {
      Student = _context.Students
        .Include(s => s.Dept)
        .Where(s => s.Std_Id == id)
        .FirstOrDefault();

      Depts = _context.Departments
        .Select(s => s.Dept_Name)
        .ToList();
    }

    public IActionResult OnPost()
	  {
      var old = _context.Students
        .Where(s => s.Std_Id == Student.Std_Id)
        .FirstOrDefault();
      _context.Students.Remove(old);


      Department dept = _context.Departments
        .Where(d => d.Dept_Name == Student.Dept.Dept_Name)
        .FirstOrDefault();
      Student.Dept = dept;
      Student.Dept_Id = dept.Dept_Id;
      _context.Students.Add(Student);

      _context.SaveChanges();

      return RedirectToPage("Std");
    }
  }
}
