using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.RazorPages;

namespace MyApp.Pages
{
  public class CreateModel : PageModel
  {
    [BindProperty]
    public Student Student { get; set; }
    public List<string> Depts { get; set; }

    private readonly ExamdbContext _context;

    public CreateModel(ExamdbContext context)
		{
      _context = context;
		}

    public void OnGet()
    {
      Depts = _context.Departments
        .Select(s => s.Dept_Name)
        .ToList();
    }

    public IActionResult OnPost()
    {
      Department dept = _context.Departments
        .Where(d => d.Dept_Name == Student.Dept.Dept_Name)
        .FirstOrDefault();
      Student.Dept = dept;
      Student.Dept_Id = dept.Dept_Id;
      _context.Students.Add(Student);

      _context.SaveChanges();

      return RedirectToPage("Std");
    }
  }
}
