using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.RazorPages;

namespace MyApp.Pages
{
  public class DeleteModel : PageModel
  {
    public IActionResult OnGet(int id)
    {
      ExamdbContext db = new ExamdbContext();
      var std = db.Students
        .Where(s => s.Std_Id == id)
        .FirstOrDefault();
      db.Students.Remove(std);
      db.SaveChanges();
      return RedirectToPage("Std");
    }
  }
}
