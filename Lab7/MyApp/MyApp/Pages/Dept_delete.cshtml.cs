using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.RazorPages;

namespace MyApp.Pages
{
  public class Dept_deleteModel : PageModel
  {
    private readonly ExamdbContext _context;

    public Dept_deleteModel(ExamdbContext context)
    {
      _context = context;
    }

    public IActionResult OnGet(int id)
    {
      Department dept = _context.Departments
        .Where(d => d.Dept_Id == id)
        .FirstOrDefault();

      _context.Departments.Remove(dept);
      _context.SaveChanges();
      return RedirectToPage("Dept");
    }
  }
}
